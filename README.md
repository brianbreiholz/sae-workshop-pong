# SAE-Workshop-Pong

SAE Workshop 2019

The classic pong game implemented in Javascript (p5.js)



Links mentioned:

p5.js: https://p5js.org/reference/
Coding Train: https://www.youtube.com/channel/UCvjgXvBlbQiydffZU7m1_aw

three.js : https://threejs.org/
WebVR: https://webvr.info/   &   https://aframe.io/