// ===================================================================================
// game variables
let canvasWidth = 800;
let canvasHeight = 400;

let playerWidth = 20;
let playerHeight = 80;

let ballPosition = {x : canvasWidth / 2, y: canvasHeight / 2};
let ballDirection = {x : 1, y: 1};

let player1Position = {x: 0, y: canvasHeight / 2};
let player2Position = {x: canvasWidth - playerWidth, y: canvasHeight / 2};

let backgroundColor = 0;
let player1Color = [230, 57, 70];
let player2Color = [51, 120, 253];

let ballRadius = 8;

let gameIsPaused = false;
let gameSpeed = 5;

let ballSpeed = 1;
let playerSpeed = 5;

let playerScores = {
  player1: 0,
  player2: 0
};

let necessaryPointsForWin = 3;
let gameOver = false;
let playerThatWon = null;


// ===================================================================================


// pause function - first call pauses the game, next call continues the game
function pauseGame() {
  gameIsPaused = !gameIsPaused;

  if (gameIsPaused) {
    noLoop(); // p5 function to pause drawing
  }
  else {
    loop(); // p5 function to resume drawing
  }
}


function handleKeyEvents(evt) {

  // check which key was pressed and choose an action accordingly
  switch (evt.key) {
    // w and s keys move player 1
    case 'w':
      if (player1Position.y > 0) {
        player1Position.y = player1Position.y - (playerSpeed * gameSpeed);
      }
      break;
    case 's':
      if ((player1Position.y + playerHeight) < canvasHeight) {
        player1Position.y = player1Position.y + (playerSpeed * gameSpeed);
      }
      break;
    // ---------------------------------------------------------

    // up and down keys move player 2
    case 'ArrowUp':
      if (player2Position.y > 0) {
        player2Position.y = player2Position.y - (playerSpeed * gameSpeed);
      }
      break;
    case 'ArrowDown':
      if ((player2Position.y + playerHeight) < canvasHeight) {
        player2Position.y = player2Position.y + (playerSpeed * gameSpeed);
      }
      break;
    // ---------------------------------------------------------

    // space bar pauses the game
    case ' ':
      pauseGame();
      break;

    // default case (any other key) - do nothing
    default:
      break;
    // ---------------------------------------------------------


  }
}

// bind a function pointer - handleKeyEvents will be called every time a key is pressed
window.addEventListener('keydown', handleKeyEvents);





// ===================================================================================
// functions that update the ball

// reset the ball position - which player scored last, determines the start ballDirection of the ball
function resetBall(lastPlayerToScore) {
  ballPosition = {x : canvasWidth/2, y: canvasHeight / 2};

  // if player 1 scored last round, player 1 gets to start the ball
  if (lastPlayerToScore === 2) {
    ballDirection = {x : 1, y: 1};
  }

  // if player 1 scored last round, player 2 gets to start the ball
  if (lastPlayerToScore === 1) {
    ballDirection = {x : -1, y: 1};
  }
}

// move the ball by updating its position
function moveBall() {
  ballPosition.x = ballPosition.x + (ballDirection.x * ballSpeed);
  ballPosition.y = ballPosition.y + (ballDirection.y * ballSpeed);
}




// ===================================================================================
// game checks - different functions that check if a player hit a ball,
// if a player scored or if the ball hit the top or bottom wall


function checkForWin() {

  if (playerScores.player1 === necessaryPointsForWin) {
    gameOver = true;
    playerThatWon = 1;
    setupWinAnimation();
  }

  if (playerScores.player2 === necessaryPointsForWin) {
    gameOver = true;
    playerThatWon = 2;
    setupWinAnimation();
  }

  return gameOver;
}


function checkForPoint() {
  // check if a player scored a point by hitting the opponents side
  // if so, increase the score of that player and reset the ball
  let someOneScored = false;
  let whichPlayerScored = null;

  if (ballPosition.x >= player2Position.x) { // player 1 scored
    playerScores.player1 = playerScores.player1 + 1;
    someOneScored = true;
    whichPlayerScored = 1;
  }

  if (ballPosition.x < (player1Position.x + playerWidth)) { // player 2 scored
    playerScores.player2 = playerScores.player2 + 1;
    someOneScored = true;
    whichPlayerScored = 2;
  }

  // if a player scored, reset the ball
  if (someOneScored) {
    resetBall(whichPlayerScored);
    // if the game is not won yet, pause the game until the other player makes his move
    if (checkForWin() === false) {
      pauseGame();
    }
  }
}



function checkForWalls() {
  // check if the ball hits the top or bottom wall
  if ((ballPosition.y > canvasHeight) || (ballPosition.y < 0)) {
    ballDirection.y = ballDirection.y * (-1);
  }
}


// check if a player hits the ball and updated the ball ballDirection accordingly
function checkIfPlayerHitsBall() {

  // check for player 1

  let hitboxPlayer1 = {
    border: player1Position.x + playerWidth,
    ymin : player1Position.y,
    ymax: (player1Position.y + playerHeight)
  };


  // check if the ballPosition is equal to player border and inside the y range
  if ((ballPosition.x - ballRadius) === hitboxPlayer1.border &&
     ((ballPosition.y + ballRadius) >= hitboxPlayer1.ymin) &&
     ((ballPosition.y - ballRadius) <= hitboxPlayer1.ymax)) {
        ballDirection.x = ballDirection.x * (-1);
  }

  // ---------------------------------------------------------------------

  // check for player 2

  let hitboxPlayer2 = {
    border: player2Position.x,
    ymin : player2Position.y,
    ymax: (player2Position.y + playerHeight)
  };

  // check if the ballPosition is equal to player border and inside the y range
  if ((ballPosition.x + ballRadius) === hitboxPlayer2.border &&
    ((ballPosition.y + ballRadius) >= hitboxPlayer2.ymin) &&
    ((ballPosition.y - ballRadius) <= hitboxPlayer2.ymax)) {
    ballDirection.x = ballDirection.x * (-1);
  }
}


// each call updates the the whole game
function updateGame() {
  // check if a player won the game
  checkForWin();
  // check if a player hit the ball
  checkIfPlayerHitsBall();
  // check if the top or bottom wall was hit
  checkForWalls();
  // check if someone scored a point by hitting the opponents wall
  checkForPoint();
  // move the ball
  moveBall();
}

// ===================================================================================
// all of our personal drawing functions

function drawBall() {
  ellipse(ballPosition.x, ballPosition.y, ballRadius * 2, ballRadius * 2);
}


function drawPlayers() {
  textSize(24);

  // draw player 1
  stroke(color(player1Color)); // stroke determines the color of lines (or outlines to elements like ellipses)
  fill(color(player1Color)); // fill determines the inner color of an element
  line(player1Position.x + playerWidth, 0, player1Position.x + playerWidth, canvasHeight);
  rect(player1Position.x, player1Position.y, playerWidth, playerHeight);
  text(playerScores.player1, 35, 35);

  // draw player 2
  stroke(color(player2Color));
  fill(color(player2Color));
  line(player2Position.x, 0, player2Position.x, canvasHeight);
  rect(player2Position.x, player2Position.y, playerWidth, playerHeight);
  stroke(color(player2Color));
  text(playerScores.player2, canvasWidth - 50, 35);


  noStroke();
  fill(255);
}



// draws all game elements - namely ball, players and goal lines
function drawGameElements() {
  drawBall();
  drawPlayers();
}



// ===================================================================================

// p5.js setup and draw

// setup is called only once - before draw

function setup() {
  createCanvas(canvasWidth, canvasHeight); // create a canvas html element for drawing
}


// draw is continuously called every frame - everything inside it will be each frame
function draw() {

    // if the game is over draw a winning animation and display the winning player
    if (gameOver) {
      drawWinAnimation(playerThatWon);
    }

    // otherwise keep playing the game
    else {
      for (let i = 0; i < gameSpeed; i++) {
        // redraw the background color
      background(backgroundColor);
      // update the game
      updateGame();
      // redraw all (updated) game elements
      drawGameElements();
    }
  }
}
